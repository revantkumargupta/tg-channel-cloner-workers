# TG Channel Cloner Workers

NOTE: Bot must be added to Both Channels, Write Permission into Destination Channel

## Workers.js File is used to Clone Old Messages Only

* First Make KV Namespace
* then add values as mentioned in photo

![https://i.imgur.com/W8bJmBi.png](https://i.imgur.com/W8bJmBi.png)

* then bind it to your Worker

![https://i.imgur.com/nb8XPbC.png](https://i.imgur.com/nb8XPbC.png)

* then start cron job

![https://i.imgur.com/XJyMszm.png](https://i.imgur.com/XJyMszm.png)

* then paste the script and add values
* then open the cloudflare workers URL once to setup webhook
* then go to bot and use commands as given below

#### Commands List

* /startbot
* /stopbot
* /status
* /from_channel <CHANNELIDHERE>
* /to_channel <CHANNELIDHERE>
* /from_message <MessageID>
* /to_message <MessageID>

## auto_forward_new_messages.js is for New Messages Auto Forwarding Service

* Paste Script and add values
* then open the cloudflare workers URL once to setup webhook

## Disclaimer

* I just wrote the code, didn't had time to clean, but it works.
* I'm not responsible for any abuse, handle with care and don't abuse.
* The Code is avaiable with License.
